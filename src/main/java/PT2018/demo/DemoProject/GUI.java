package PT2018.demo.DemoProject;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class GUI extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JLabel lbl1;
	private JLabel lbl2;
	private JLabel lbl3;
	JTextField tf1;
	JTextField tf2;
	JTextField tf3;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;

	public GUI() {
		frame = new JFrame();
		frame.setBounds(300, 300, 500, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.YELLOW);

		lbl1 = new JLabel("1.Primul polinom");
		lbl1.setForeground(Color.RED);
		lbl1.setBounds(20, 15, 150, 25);
		frame.getContentPane().add(lbl1);

		lbl2 = new JLabel("2.Al doilea polinom");
		lbl2.setForeground(Color.ORANGE);
		lbl2.setBounds(20, 60, 190, 50);
		frame.getContentPane().add(lbl2);

		lbl3 = new JLabel("3.Rezultatul");
		lbl3.setForeground(Color.BLUE);
		lbl3.setBounds(20, 100, 230, 75);
		frame.getContentPane().add(lbl3);

		tf1 = new JTextField();
		tf1.setBounds(40, 40, 200, 30);
		frame.getContentPane().add(tf1);
		tf1.setColumns(30);

		tf2 = new JTextField();
		tf2.setBounds(40, 95, 200, 30);
		frame.getContentPane().add(tf2);
		tf2.setColumns(30);

		tf3 = new JTextField();
		tf3.setBounds(40, 150, 200, 30);
		frame.getContentPane().add(tf3);
		tf3.setColumns(30);

		btn1 = new JButton("Adunare");
		btn1.setBounds(260, 20, 130, 40);
		frame.getContentPane().add(btn1);
		btn1.addActionListener(this);

		btn2 = new JButton("Scadere");
		btn2.setBounds(260, 70, 130, 40);
		frame.getContentPane().add(btn2);
		btn2.addActionListener(this);

		btn3 = new JButton("Inmultire");
		btn3.setBounds(260, 120, 130, 40);
		frame.getContentPane().add(btn3);
		btn3.addActionListener(this);

		btn4 = new JButton("Impartire");
		btn4.setBounds(260, 170, 130, 40);
		frame.getContentPane().add(btn4);
		btn4.addActionListener(this);

		btn5 = new JButton("Derivare");
		btn5.setBounds(260, 220, 130, 40);
		frame.getContentPane().add(btn5);
		btn5.addActionListener(this);

		btn6 = new JButton("Integrare");
		btn6.setBounds(260, 270, 130, 40);
		frame.getContentPane().add(btn6);
		btn6.addActionListener(this);

		frame.setTitle("TEMA1");
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent arg0) {

		Object source = arg0.getSource();
		String s1 = tf1.getText();
		String s2 = tf2.getText();
		Polinom p1 = new Polinom(s1);
		Polinom p2 = new Polinom(s2);

		if (source == btn1) {
			Polinom p3 = p1.add(p2);
			p3.stergereZerouri();
			Collections.sort(p3.L, new SortareDupaGrad());
			tf3.setText(p3.afis());

		} else if (source == btn2) {
			Polinom p4 = p1.sub(p2);
			p4.stergereZerouri();
			Collections.sort(p4.L, new SortareDupaGrad());
			tf3.setText(p4.afis());
		} else if (source == btn3) {
			Polinom p5 = p1.mul(p2);
			p5.stergereZerouri();
			Collections.sort(p5.L, new SortareDupaGrad());
			tf3.setText(p5.afis());
		} else if (source == btn5) {
			Polinom p6 = p1.deriv();
			p6.stergereZerouri();
			Collections.sort(p6.L, new SortareDupaGrad());
			tf3.setText(p6.afis());
		} else if (source == btn6) {
			Polinom p7 = p1.integ();
			p7.stergereZerouri();
			Collections.sort(p7.L, new SortareDupaGrad());
			tf3.setText(p7.afis());
		}
	}

	public static void main(String[] args) {
		GUI g = new GUI();
	}

}
