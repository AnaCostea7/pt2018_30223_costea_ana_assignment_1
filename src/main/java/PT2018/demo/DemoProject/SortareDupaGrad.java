package PT2018.demo.DemoProject;

import java.util.Comparator;

public class SortareDupaGrad implements Comparator<Monom> {
	
	public int compare(Monom a, Monom b)
	{
		return b.exponent-a.exponent;
	}
}
