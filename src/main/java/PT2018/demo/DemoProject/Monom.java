package PT2018.demo.DemoProject;

public class Monom {
	double coeficient;
	int exponent;
	
	public Monom(double coeficient, int exponent) {
		this.coeficient = coeficient;
		this.exponent = exponent;
	}
	
	public double getCoef() {
		return coeficient;
	}

	public void setCoef(double coeficient) {
		this.coeficient = coeficient;
	}

	public int getExp() {
		return exponent;
	}

	public void setExp(int exponent) {
		this.exponent = exponent;
	}

	public Monom add(Monom m) {
		if(this.exponent == m.exponent) {
			this.coeficient += m.coeficient;
		}
		return this;
	}
	
	public Monom sub(Monom m) {
		if(this.exponent == m.exponent) {
			this.coeficient -= m.coeficient;
		}
		return this;
	}
	
	public Monom mul(Monom m) {
			this.coeficient = this.coeficient*m.coeficient;
			this.exponent += m.exponent;
		return this;
	}
	
	//public Monom div(Monom m) {
		
	//}
	
	public Monom deriv() {
		this.coeficient *= this.exponent;
		this.exponent--;
		return this;
	}
	
	public Monom integ() {
		this.exponent++;
		this.coeficient /= this.exponent;
		return this;
	}
	
	public String toString() {
		return coeficient + "x^" + exponent;
	}
	
}
