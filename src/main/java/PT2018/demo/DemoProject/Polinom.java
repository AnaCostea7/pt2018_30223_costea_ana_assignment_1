package PT2018.demo.DemoProject;

import java.util.ArrayList;
import java.util.regex.*;

public class Polinom {
	public ArrayList<Monom> L = new ArrayList<Monom>();

	public Polinom() {
		L = new ArrayList<Monom>();
	}

	public void addMonom(Monom m) {
		L.add(m);
	}

	public Polinom(String a) {
		Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
		Matcher m = p.matcher(a);
		while (m.find()) {
			Monom mo = new Monom(Double.parseDouble(m.group(1)), Integer.parseInt(m.group(2)));
			this.addMonom(mo);
			mo = null;
		}
	}

	public Polinom add(Polinom A) {


		Polinom result = new Polinom();

		for (Monom m : this.L) {
			for (Monom m2 : A.L) {
				if (m.getExp() == m2.getExp() && !m.equals(m2)) {
					Monom res = new Monom(m.getCoef() + m2.getCoef(), m.getExp());
					result.L.add(res);
					res = null;
				}
			}
		}
		int ok = 0;
		for (Monom m : this.L) {
			for (Monom m2 : A.L) {
				if (m.getExp() == m2.getExp()) {
					ok = 1;
				}
			}
			if (ok == 0) {
				Monom res = new Monom(m.getCoef(), m.getExp());
				result.L.add(res);
				res = null;
			}
			ok = 0;
		}

		for (Monom m : A.L) {
			for (Monom m2 : this.L) {
				if (m.getExp() == m2.getExp()) {
					ok = 1;
				}
			}

			if (ok == 0) {
				Monom res = new Monom(m.getCoef(), m.getExp());
				result.L.add(res);
				res = null;
			}

			ok = 0;
		}
		return result;
	}


	public Polinom sub(Polinom A) {

		Polinom result = new Polinom();
		for (Monom m : this.L) {
			for (Monom m2 : A.L) {
				if (m.getExp() == m2.getExp() && !m.equals(m2)) {
					Monom res = new Monom(m.getCoef() - m2.getCoef(), m.getExp());
					result.L.add(res);
					res = null;

				}
			}
		}

		int ok = 0;

		for (Monom m : this.L) {
			for (Monom m2 : A.L) {
				if (m.getExp() == m2.getExp()) {
					ok = 1;
				}
			}

			if (ok == 0) {
				Monom res = new Monom(m.getCoef(), m.getExp());
				result.L.add(res);
				res = null;
			}
			ok = 0;
		}

		for (Monom m : A.L) {
			for (Monom m2 : this.L) {
				if (m.getExp() == m2.getExp()) {
					ok = 1;
				}
			}

			if (ok == 0) {
				Monom res = new Monom(m.getCoef()*(-1), m.getExp());
				result.L.add(res);
				res = null;
			}
			ok = 0;
		}

		return result;
	}

	 public Polinom mul(Polinom A) {
		 
		 Polinom result= new Polinom();
		 
		 for(Monom m1: A.L)
		 {
			 for(Monom m2: this.L)
			 {
				 Monom m3=new Monom(m1.getCoef()*m2.getCoef(), m1.getExp()+m2.getExp());
				 result.L.add(m3);
				 m3=null;
			 }
		 }
		 
		 for(Monom m1: result.L)
		 {
			 for(Monom m2: result.L)
			 {
				 if(m1.getExp()==m2.getExp() && !m1.equals(m2))
				 {
					 m1.setCoef(m1.getCoef()+m2.getCoef());
					 m2.setCoef(0);
				 }
						 
			 }
		 }
		 
		 return result;

	 }

	public Polinom deriv() {
		Polinom result = new Polinom();
		for (Monom m : this.L) {
			Monom res = new Monom(m.getCoef() * m.getExp(), m.getExp() - 1);
			result.L.add(res);
			res = null;
		}
		return result;
	}

	public Polinom integ() {
		Polinom result = new Polinom();
		for (Monom m : this.L) {
			m.exponent++;
			Monom res = new Monom(m.getCoef() / m.getExp(), m.getExp());
			result.L.add(res);
			res = null;
		}
		return result;
	}

	public String afis() {
		String afis = "";
		int count = 0;

		for (Monom m : this.L) {

			if (m.getCoef() > 0 && !m.equals(this.L.get(0))) {
				afis = afis + "+";
			}

			if (m.getCoef() != 0 && (m.getCoef() != 1 || count == L.size() - 1)) {
				if (m.getCoef() != -1 || count == this.L.size() - 1) {
					afis = afis + m.getCoef();
				} else {
					afis = afis + "-";
				}
			}


			if (m.getCoef() != 0 && m.getExp() != 0) {
				afis = afis + "x";

				if (m.getExp() != 1 && m.getExp() != -1) {
					afis = afis + "^" + m.getExp();
				}
			}
			
			if (afis.equals("")) {
				afis = "0";
			}

			count++;
		}

		return afis;
	}
	
	public void stergereZerouri()
	{
		for(int i=0; i<this.L.size(); i++)
		{
			if(this.L.get(i).getCoef()==0)
			{
				this.L.remove(i);
			}
		}
	}
}
