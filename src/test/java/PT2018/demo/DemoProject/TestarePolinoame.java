package PT2018.demo.DemoProject;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

public class TestarePolinoame {

	@Test
	public void testareAdaugare()
	{
		Polinom p1=new Polinom();
		Polinom p2=new Polinom();
		
		Polinom rezultatAsteptat=new Polinom();
		Polinom rezultatCalculat=new Polinom();
		
		///De exemplu adaugam x^3+5x+9 cu 2x^2-2x+3
		///Rezultatul ar trebui sa fie x^3+2x^2+3x+12
		
		p1.L.add(new Monom(1,3));
		p1.L.add(new Monom(5,1));
		p1.L.add(new Monom(9,0));
		
		p2.L.add(new Monom(2,2));
		p2.L.add(new Monom(-2,1));
		p2.L.add(new Monom(3,0));
		
		rezultatAsteptat.L.add(new Monom(1,3));
		rezultatAsteptat.L.add(new Monom(2,2));
		rezultatAsteptat.L.add(new Monom(3,1));
		rezultatAsteptat.L.add(new Monom(12,0));
		
		rezultatCalculat=p1.add(p2);
		Collections.sort(rezultatCalculat.L,new SortareDupaGrad());
		
		Assert.assertEquals(rezultatAsteptat.afis(), rezultatCalculat.afis());
		
		
	}
	
	@Test
	public void testareScadere()
	{
		Polinom p1=new Polinom();
		Polinom p2=new Polinom();
		
		Polinom rezultatAsteptat=new Polinom();
		Polinom rezultatCalculat=new Polinom();
		
		///De exemplu scadem din x^3+7x+3   3x^2-x+2
		///Rezultatul ar trebui sa fie x^3-3x^2+8x+1
		
		p1.L.add(new Monom(1,3));
		p1.L.add(new Monom(7,1));
		p1.L.add(new Monom(3,0));
		
		p2.L.add(new Monom(3,2));
		p2.L.add(new Monom(-1,1));
		p2.L.add(new Monom(2,0));
		
		rezultatAsteptat.L.add(new Monom(1,3));
		rezultatAsteptat.L.add(new Monom(-3,2));
		rezultatAsteptat.L.add(new Monom(8,1));
		rezultatAsteptat.L.add(new Monom(1,0));
		
		rezultatCalculat=p1.sub(p2);
		Collections.sort(rezultatCalculat.L,new SortareDupaGrad());
		
		Assert.assertEquals(rezultatAsteptat.afis(), rezultatCalculat.afis());
		
		
	}
	
	@Test
	public void testareInmultire()
	{
		Polinom p1=new Polinom();
		Polinom p2=new Polinom();
		
		Polinom rezultatAsteptat=new Polinom();
		Polinom rezultatCalculat=new Polinom();
		
		///De exemplu inmultim 2x^2+2x cu 3x+3
		///Rezultatul ar trebui sa fie 6x^3+12x^2+6x
		
		p1.L.add(new Monom(2,2));
		p1.L.add(new Monom(2,1));
		
		p2.L.add(new Monom(3,1));
		p2.L.add(new Monom(3,0));
		
		rezultatAsteptat.L.add(new Monom(6,3));
		rezultatAsteptat.L.add(new Monom(12,2));
		rezultatAsteptat.L.add(new Monom(6,1));
		
		rezultatCalculat=p1.mul(p2);
		Collections.sort(rezultatCalculat.L,new SortareDupaGrad());
		
		Assert.assertEquals(rezultatAsteptat.afis(), rezultatCalculat.afis());
		
		
	}
	
	@Test
	public void testareDerivare()
	{
		Polinom p1=new Polinom();
		
		Polinom rezultatAsteptat=new Polinom();
		Polinom rezultatCalculat=new Polinom();
		
		///De exemplu derivam 6x^3+4x^2+6x
		///Rezultatul ar trebui sa fie 18x^2+8x+6
		
		p1.L.add(new Monom(6,3));
		p1.L.add(new Monom(4,2));
		p1.L.add(new Monom(6,1));
		
		
		rezultatAsteptat.L.add(new Monom(18,2));
		rezultatAsteptat.L.add(new Monom(8,1));
		rezultatAsteptat.L.add(new Monom(6,0));
		
		rezultatCalculat=p1.deriv();
		Collections.sort(rezultatCalculat.L,new SortareDupaGrad());
		
		Assert.assertEquals(rezultatAsteptat.afis(), rezultatCalculat.afis());
		
		
	}
	
	@Test
	public void testareIntegrare()
	{
		Polinom p1=new Polinom();
		
		Polinom rezultatAsteptat=new Polinom();
		Polinom rezultatCalculat=new Polinom();
		
		///De exemplu integram 3x^2+2x
		///Rezultatul ar trebui sa fie x^3+x^2
		
		p1.L.add(new Monom(3,2));
		p1.L.add(new Monom(2,1));
		
		
		rezultatAsteptat.L.add(new Monom(1,3));
		rezultatAsteptat.L.add(new Monom(1,2));
		
		rezultatCalculat=p1.integ();
		Collections.sort(rezultatCalculat.L,new SortareDupaGrad());
		
		Assert.assertEquals(rezultatAsteptat.afis(), rezultatCalculat.afis());
		
		
	}

}
